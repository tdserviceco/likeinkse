<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php get_template_part('parts/part', 'favicons') ?>
  <link rel="manifest" href="<?php echo get_template_directory_uri(). '/img/manifest.json'?>">
  <link rel="mask-icon" href="<?php echo get_template_directory_uri(). '/img/safari-pinned-tab.svg'?>" color="<?php echo get_field('mobile-theme-color', 'option') ?>">
  <meta name="theme-color" content="<?php echo get_field('mobile-theme-color', 'option') ?>">
  <?php wp_head() ?>
  <?php get_template_part('parts/part', 'scripts-header'); ?>
</head>
<body <?php body_class() ?>>
<div class="sale-pitch">
  <div class="pitch">
    <p><?php _e('Snabba leveranser, Produktion i Malmö, Flera betalsätt, Fri frakt','likeink'); ?>.</p>
  </div>
</div>
<?php if(!is_page('cart')): ?>
<header class="desktop">
  <div class="menu-container">
    <?php get_template_part( 'parts/part', 'logotype' ) ?>
    <?php likeink_custom_rules::wp_menu(); ?>
  </div>  
</header>

<header class="mobile">
  <div class="menu-container">
    <?php get_template_part( 'parts/part', 'logotype' ) ?>
    <div class="menu-button">
      <input id="clickme" type="checkbox" class="menu">
      <label for="clickme" id="menu">
        <span class="bar-1"></span>
        <span class="bar-2"></span>
        <span class="bar-3"></span>
      </label>
    </div>
  </div>  
  <?php likeink_custom_rules::wp_menu(); ?>
</header>
<?php endif; ?>