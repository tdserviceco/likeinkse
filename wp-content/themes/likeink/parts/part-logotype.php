<?php
$acf_field_logotype = get_field('logotype', 'option');
$logotype_80x80 = $acf_field_logotype['sizes']['thumbnail-small'];
$stock_img = get_template_directory_uri() . '/img/logo.png';
if(!empty($logotype_80x80)): ?>
  <a href="/"><img src='<?php echo $logotype_80x80 ?>' alt='Logotype'></a>
<?php else: ?>
  <a href="/"><img src="<?php echo $stock_img; ?>" class="logotype" alt="Logotype"></a>  
<?php endif; ?>