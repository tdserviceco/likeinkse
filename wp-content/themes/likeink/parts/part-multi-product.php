<?php

$args = [
  'post_per_page'          => 8,
  'post_type'              => array('product'),
  'tax_query' => array(
    array(
      'taxonomy' => 'product_cat',
      'field' => 'slug',
      'terms' => 'custom'
    )
  )
];

// the query
$the_query = new WP_Query( $args ); ?>

<?php if ( $the_query->have_posts() ) : ?>
  <div class="container columns-4">
	<!-- pagination here -->

	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <?php
    if(has_post_thumbnail($post->ID)):
      ?>
      <div class="product-thumbnail">
        <?php the_post_thumbnail(); ?>
        <button type="button" class="add-to-cart-button multiproduct-button" data-pid="<?php echo $post->ID; ?>"><?php _e('Add to cart','likeink')?></button>
      </div>
      <?php
    endif;
    ?>
	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
  </div>
<?php endif; ?>