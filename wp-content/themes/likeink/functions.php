<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

class likeink_setup {
  static function hooks() {
    add_action( 'wp_enqueue_scripts', array(__CLASS__, 'scripts') ); 
    add_action( 'after_setup_theme', array(__CLASS__, 'load_theme_textdomain') );
    add_action( 'wp_footer', array(__CLASS__, 'add_ajax_script') );
    add_action( 'after_setup_theme', array( __CLASS__, 'theme_support' ) );
    add_action( 'after_setup_theme', array( __CLASS__, 'option_menu' ), 15, 1 );
    add_action( 'init', array( __CLASS__, 'add_menu' ) );
    add_filter( 'acf/settings/load_json', array( __CLASS__, 'acf_json_load_point' ) );
    add_filter( 'acf/settings/save_json', array( __CLASS__, 'my_acf_json_save_point' ) );
    add_filter( 'upload_mimes', array( __CLASS__, 'cc_mime_types' ) );
    add_action( 'woocommerce_cart_calculate_fees', array( __CLASS__, 'discount_based_on_total' ) , 25, 1 );
  }
  
  static function discount_based_on_total( $cart ) {
  
    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
    
    $cart_contents_count = WC()->cart->cart_contents_count;
    /*
    foreach ( WC()->cart->get_cart() as $cart_item ) { 
        if($cart_item['product_id'] == $targeted_id ){
            $qty =  $cart_item['quantity'];
            break; // stop the loop if product is found
        }
    }
    */
    // Displaying the quantity if targeted product is in cart
    $total = $cart->cart_contents_total;

    // Percentage discount
    if( $cart_contents_count == 1 ):
      $discount = 0;
    endif;

    if( $cart_contents_count == 2 ):
      $total_calc = 98 * 2;
      $discount = (6 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count == 3 ):
      $total_calc = 98 * 3;
      $discount = (12 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count == 4 ):
      $total_calc = 98 * 4;
      $discount = (32 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count == 5 ):
      $total_calc = 98 * 5;
      $discount = (40 / $total_calc) * $total_calc; 
    endif;

    if( $cart_contents_count == 6 ):
      $total_calc = 98 * 6;
      $discount = (78 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count == 7 ):
      $total_calc = 98 * 7;
      $discount = (91 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count == 8 ):
      $total_calc = 98 * 8;
      $discount = (120 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count == 9 ):
      $total_calc = 98 * 9;
      $discount = (135 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 10 ):
      $total_calc = 98 * 10;
      $discount = (180 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 20 ):
      $total_calc = 98 * 20;
      $discount = (380 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 30 ):
      $total_calc = 98 * 30;
      $discount = (600 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 40 ):
      $total_calc = 98 * 40;
      $discount = (920 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 50 ):
      $total_calc = 98 * 50;
      $discount = (1250 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 100 ):
      $total_calc = 98 * 100;
      $discount = (2900 / $total_calc) * $total_calc;
    endif;

    if( $cart_contents_count >= 200 ):
      $total_calc = 98 * 200;
      $discount = (6600 / $total_calc) * $total_calc;
    endif;

    // Add the discount
    $cart->add_fee( __('Discount', 'likeink'), -$discount );
  }

  static function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }

  static function my_acf_json_save_point( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $path;  
  }

  static function acf_json_load_point( $paths ) {
   // remove original path (optional)
   unset($paths[0]);
        
   // append path
   $paths[] = get_stylesheet_directory() . '/acf-json';
        
   // return
   return $paths;
  }

  static function scripts() {
    wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.css');
    wp_enqueue_style( 'awesome-fonts', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css');
    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js' , array('jquery') , true );
    wp_enqueue_script('cookie-law-text', get_template_directory_uri() . '/js/cookie-text.js', array('jquery') , true);
  }

  static function add_ajax_script() {
    echo '<script type="text/javascript">const ajaxurl = "' . admin_url( 'admin-ajax.php' ) . '";</script>';
  }

  static function add_menu() {
		register_nav_menu( 'header', __( 'Header Menu' ) );
	}

  static function load_theme_textdomain() {
		load_theme_textdomain( 'likeink', get_template_directory() . '/languages' );
  }
  
	static function option_menu() {
    if ( function_exists( 'acf_add_options_page' ) ) 
    {
			acf_add_options_page( 
        array(
          'page_title' => __( 'Theme Options', 'likeink' ),
          'menu_title' => __( 'Theme Options', 'likeink' ),
          'menu_slug'  => 'theme_master',
        ) 
      );
		}
  }

  static function theme_support() {
		$html5 = array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption'
		);
		add_theme_support( 'post-formats', $html5 );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'woocommerce' );
    
    //Image sizes
    add_image_size('hero', 1800, 800, false);
    add_image_size('thumbnail-small', 80, 80, true);
    add_image_size('avartar-big', 300, 300, true);
    add_image_size('avartar-small', 150, 150, true);
    add_image_size('favicon-large', 180, 180, false);
    add_image_size('favicon-medium', 32, 32, false);
    add_image_size('favicon-small', 16, 16, false);

	}
}

class likeink_security extends likeink_setup {
  static function security_check() {
		parent::hooks();
		add_action( 'init', array( __CLASS__, 'disable_stuff' ) );
	}

  static function disable_stuff() {
    //Disable Emoji, Move to optimization snippet...
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

    //Disable WLW, unless Windows Live Writer is used.
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'rsd_link' );

    //Disable XML RPC
    add_filter( 'xmlrpc_enabled', '__return_false' );

    //Remove extra spacing in WYSIWYG
    remove_filter( 'the_content', 'wpautop' ); 
    remove_filter( 'the_excerpt', 'wpautop' );

    //Disable feed, unless Comment Feeds are used.
    add_action( 'do_feed', 'wp_die', 1 );
    add_action( 'do_feed_rdf', 'wp_die', 1 );
    add_action( 'do_feed_rss', 'wp_die', 1 );
    add_action( 'do_feed_rss2', 'wp_die', 1 );
    add_action( 'do_feed_atom', 'wp_die', 1 );
  }
}

class likeink extends likeink_security {
  static function startup() {
    parent::security_check();
  }
}

likeink::startup();

class likeink_custom_rules {
  static function wp_menu($args = []) {
    $args = [
      'theme_location' => 'header', 
      'container' => 'nav'
    ];
    wp_nav_menu($args);
  }

  static function mini_cart() { 
    if(is_page('cart')):

      $markup = 
      '<div class="mini-cart" style="display:none">
        <div class="mini-cart__container">
          <div class="mini-cart__badge">
            <span class="mini-cart__amount-of-product"><strong>'.WC()->cart->get_cart_contents_count().'</strong></span>
          </div>
        </div>
      </div>';
    else: 
    $markup = 
    '<a href="'.home_url().'/cart" class="mini-cart">
      <div class="mini-cart__container">
        <div class="mini-cart__badge">
          <span class="mini-cart__amount-of-product"><strong>'.WC()->cart->get_cart_contents_count().'</strong></span>
        </div>
      </div>
    </a>';
    endif;
    echo $markup;
  }
}