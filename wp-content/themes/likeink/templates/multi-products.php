<?php
$title = get_sub_field('title');
$background = get_sub_field('background-image');

if(!empty($background)):
  $choice = 'background-image: url('.$background['url'].')';
else:
  $choice = '';
endif;
?>

<section id="multi-products" class="multi-products" style="<?php echo $choice; ?>">
  <div class="center">
    <div class="container">
      <div class="headline">
        <h2 class="header2 text-center"><?php echo $title; ?></h2>
      </div>
      <div class="products">
        <?php get_template_part('parts/part','multi-product'); ?>
      </div>
    </div>
  </div>
</section>

