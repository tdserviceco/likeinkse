<?php
$title = get_sub_field('title');
$background = get_sub_field('background-image');
$info = get_sub_field('info');
if(!empty($background)):
  $choice = 'background-image: url('.$background['url'].')';
else:
  $choice = '';
endif;
?>

<section id="contact-form" class="contact-form" style="<?php echo $choice; ?>">
  <div class="center">
    <div class="container">
      <div class="headline">
        <h2 class="header2 text-center">
          <?php echo $title; ?>
        </h2>
      </div>
      <div class="columns-2">
        <div class="info">
          <?php echo $info; ?>
        </div>
        <div class="contact">
          <?php echo do_shortcode( '[contact-form-7 id="358" title="Contact form 1"]' ); ?>
        </div>
      </div>
    </div>
  </div>
</section>