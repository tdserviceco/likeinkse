<?php 
  $background = get_sub_field('background-image');
?>
<section id="faq" class="faq" style="background: url(<?php echo $background['url']; ?>)"> 
  <div class="center">
    <div class="container">
      <?php $title = get_sub_field('title'); ?>
      <div class="headline">
        <h2 class="header2 text-center">
          <?php echo $title; ?>
        </h2>
      </div>
      <?php 
      if( have_rows('faq-repeater') ):
        while ( have_rows('faq-repeater') ) : the_row();
        $question = get_sub_field('question');
        $answer = get_sub_field('answer'); ?>
        <div class="questions container">
          <div class="question">
            <h3><?php echo $question; ?></h3>
          </div>
          <div class="answer">
            <?php echo $answer; ?>
          </div>
        </div>
        <?php
        endwhile;
      endif;
      ?>
    </div>
  </div>
</section>