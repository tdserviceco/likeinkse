<?php
$background = get_sub_field('background-image');
$instructions = get_sub_field('instructions');
if(!empty($background)):
  $choice = 'background-image: url('.$background['url'].')';
else:
  $choice = '';
endif; 
?>
<section class="file-uploader target-ID" id="file-uploader" style="<?php echo $choice; ?>">
  <?php if(!empty($instructions)): ?>
    <div class="center">
      <div class="instructions">
        <?php echo $instructions; ?>
      </div>  
    </div>
  <?php endif; ?>
  <?php echo do_shortcode( '[image_sheet]' ); ?>
</section>