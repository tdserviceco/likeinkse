<?php
/**
 * Setup
 */

$title = get_sub_field('title');
$pre_title = get_sub_field('pre-title');

$color = 'rgba(0,110,140,0.85)';

if( !empty(get_sub_field('background-image')) ):
	$image = get_sub_field('background-image');
endif;
?>
<section class="hero" style="background: url(<?php echo $image['url']; ?>) no-repeat center center">
	<div class="hero-container center" style="background-color: <?php echo $color; ?>">
		<div class="content">
			<h1 class="header-1 big-letters"><?php echo $title ?></h1>
			<h2 class="header-2 small-letters"><?php echo $pre_title ?></h2>
			<div class="text-center hero-button">
				<a href="#file-uploader" class="button">Kom igång</a>
			</div>
		</div>
		
	</div>
</section>