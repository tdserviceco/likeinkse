<?php
$title = get_sub_field('title');
$info = get_sub_field('info');
$background = get_sub_field('background-image');
if(!empty($background)):
  $choice = 'background-image: url('.$background['url'].')';
else:
  $choice = '';
endif;
?>

<section id="discount" class="discount-slider" style="<?php echo $choice; ?>">
  <div class="center">
    <div class="container">  
      <div class="headline">
        <h2 class="header2 text-center">
          <?php echo $title; ?>
        </h2>
      </div>
      <div class="info">
        <?php echo $info; ?>
      </div>
      <div class="range-per-cm">
        <input type="range" class="per-cm" name="per-cm" value="1" min="1" max="200">
      </div>
      <div class="result columns-4">
        <div class="ark text-center">
          <h4><?php _e('Antal ark','likeink');?></h4>
          <span>1</span>
        </div>
        <div class="price text-center">
          <h4><?php _e('Pris','likeink');?></h4>
          <span>98kr</span>
        </div>
        <div class="discount text-center">
          <h4><?php _e('Rabatt','likeink');?></h4>
          <span>0kr</span>
        </div>
        <div class="your-price text-center">
          <h4><?php _e('Ditt pris','likeink');?></h4>
          <span>98kr</span>
        </div>
      </div>
    </div> 
  </div>
</section>