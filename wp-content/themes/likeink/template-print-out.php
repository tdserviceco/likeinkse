<?php
/** 
 * Get our order and print them out! 
 * 
 * Template Name: Print out!
 * 
*/
get_header();

$amount = $_GET['amount'];
$filename = $_GET['filename'];
$cm = $_GET['cm'];
$upload_path = wp_upload_dir();
if ( is_user_logged_in() ) { ?>
  <div class="image-print-out center">
    <?php 
      for($x = 1; $x <= $amount; $x++) {
        echo "<img class='preview-image' style='width:". ($cm/201*1000) ."%' src='".$upload_path['baseurl'].'/wp-image-sheet/'.$filename."'>";
      }
    ?>
  </div>
<?php } else {
  echo "<h1>".__('Access Denied!', 'likeink')."</h1>";  
}
get_footer(); 
?>
