
<?php 
if(!is_page('cart')):
$phone = get_field('phone-number', 'option');
$youtube = get_field('youtube', 'option');
$instagram = get_field('instagram', 'option');
$facebook = get_field('facebook', 'option');
?>
<footer>
  <div class="information-on-likeink">
    <div class="center">
      <div class="columns-3">
        <div class="left">
          <div class="contact-field">
          <h3><?php _e('Contact', 'likeink'); ?></h3>
            <?php 
              $contact = get_field('adress', 'option');
              $phone = get_field('phone-number', 'option');
              $email = get_field('email', 'option');
              echo $contact;
            ?>
            <p><?php _e('Phone','likeink');?>: <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
            <p><?php _e('Email','likeink');?>: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
          </div>
          <div class="mobile-social-media">
            <h3><?php _e('Social media' , 'likeink'); ?></h3>
            <div class="medias columns-3">
                <a href="https://www.facebook.com/LikeInkSe/"><i class="fab fa-facebook"></i></a>
                <a href="https://www.instagram.com/likeinkse/"><i class="fab fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCWwOWUYvXnzeAshocabmX7Q"><i class="fab fa-youtube"></i></a>
            </div>
          </div>
        </div>
        <div class="middle">
          <p>
          Like ink är troligtvis Sveriges största leverantör av engångstatueringar och våra nöjda kunder är allt från företag, kommuner, tryckerier, teatrar och tv-bolag till glada bröllopsgäster, barn på kalas, studenter och personer som ska tatuera sig.  Välkommen till Like ink – custom made temporary tattoos.  
          </p>
        </div>
        <div class="right">
        <?php 
          $args = ['menu' => 34];
          wp_nav_menu($args); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="desktop-social-media">
    <div class="text-center center">
      <h3><?php _e('Follow us here','likeink'); ?></h3>
      <div class="medias columns-3">
        <div class="left">
          <a href="https://www.facebook.com/LikeInkSe/"><i class="fab fa-facebook"></i></a>
        </div>
        <div class="middle">
          <a href="https://www.instagram.com/likeinkse/"><i class="fab fa-instagram"></i></a>
        </div>
        <div class="right">
          <a href="https://www.youtube.com/channel/UCWwOWUYvXnzeAshocabmX7Q"><i class="fab fa-youtube"></i></a>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php endif; ?>
<?php
$text = get_field('cookie-text', 'option');
?>

<div class="cookie-container">
  <div class="cookie-text">
    <?php echo $text; ?><button type="button" class="button accept-cookie-button"><?php _e('I accept','likeink'); ?></button>
  </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
