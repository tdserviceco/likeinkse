<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * @hooked wc_empty_cart_message - 10
 */
?>

<div class="empty-cart" style="<?php echo $background; ?>">
  <div class="center container">
    <?php 
    do_action( 'woocommerce_cart_is_empty' );
    if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
      <div class="return-to-shop center">
        <a class="button wc-backward" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( '/' ) ) ); ?>">
          <?php _e( 'Return to shop', 'woocommerce' ) ?>
        </a>
      </div>
    <?php endif; ?>
  </div>
</div>