let $ = jQuery;

const likeInkStartUp = {
  onReady: function () {
    likeInkStartUp.startUp(),
    likeInkStartUp.activeResponsiveMenu()
  },

  startUp : function() {
    /** un-comment this field if you want to start using startUp function */
    //console.log('main.js loaded');
  },

  activeResponsiveMenu : function() {
    $('#menu').on('click', function () {
      $('.bar-1').toggleClass('animate');
      $('.bar-2').toggleClass('animate');
      $('.bar-3').toggleClass('animate');
      $('.menu').toggleClass('show');
    });
  }
}

jQuery(document).ready(likeInkStartUp.onReady);
