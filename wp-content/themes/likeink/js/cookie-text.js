function setCookie(cname,cvalue,exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}

function checkCookie() {
  var accessPoint = getCookie("wp-cookie-text-active");
  if (accessPoint != "") {
      console.log("Welcome again");
      $('.cookie-container').addClass('cookie-set');
  } 
  else {
    $('.cookie-container').removeClass('cookie-set');  
    $('.accept-cookie-button').on('click', function(e){
      setCookie("wp-cookie-text-active", 1, 30);
      $('.cookie-container').addClass('cookie-set');
      console.log('Cookie saved');
    })
  }
}

$( document ).ready(function() {
  checkCookie();
});