<?php get_header(); ?>
<main> 
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php if( !empty( get_the_content() ) ): ?>
    <?php echo the_content() ?>
  <?php endif; ?>
  <?php get_template_part( 'inc/flexible-elements' ) ?>
  <?php endwhile; ?>
<?php endif; ?>
</main>
<?php get_footer(); ?>