/**
 * Global variabls
*/

var imgheight = '';
var imgwidth = '';
var minheight = '';
var minwidth = '';

const img = new Image();
let _URL = window.URL || window.webkitURL;

var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.box');
var $fileInput = $('.box input[type="file"]');

/**
 * Functions
 */

let imageSheetMainFunction = {

  onReady: function () {
    imageSheetMainFunction.startUp(),
    imageSheetMainFunction.dragNDrop(),
    imageSheetMainFunction.updateCmAndAmount(),
    imageSheetMainFunction.removeFromCart(),
    imageSheetMainFunction.multiProductAddToCart(),
    imageSheetMainFunction.getDiscountFromSlider()
  },

  getDiscountFromSlider : function() {

    $('input[name=per-cm]').on('change', function(){
      var orginalPrice = 98; 
      var range = $('.discount-slider .range-per-cm > .per-cm').val();

      $('.discount-slider .ark > span').html(range);
      imageSheetMainFunction.calculate(orginalPrice, range);
    })
  },


  calculate : function(orginalPrice, range) {
    var showDiscount = $('.discount');
    var totalPrice = '';
    var newPrice = '';
    
    if(range <= 1 ) {
      $('.your-price > span').html('98kr');
      $('.discount > span').html('0kr');
      $('.price > span').html(orginalPrice + 'kr');
      showDiscount.removeClass('discount-color');
    }
    if(range >= 2) {
      discountPrice = 6;
      totalPrice = orginalPrice * 2 - discountPrice + 'kr';
      newPrice = orginalPrice * 2 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 3) {
      discountPrice = 12;
      totalPrice = orginalPrice * 3 - discountPrice + 'kr';
      newPrice = orginalPrice * 3 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 4) {
      discountPrice = 32;
      totalPrice = orginalPrice * 4 - discountPrice + 'kr';
      newPrice = orginalPrice * 4 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 5) {
      discountPrice = 40;
      totalPrice = orginalPrice * 5 - discountPrice + 'kr';
      newPrice = orginalPrice * 5 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 6) {
      discountPrice = 78;
      totalPrice = orginalPrice * 6 - discountPrice + 'kr';
      newPrice = orginalPrice * 6 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 7) {
      discountPrice = 91;
      totalPrice = orginalPrice * 7 - discountPrice + 'kr';
      newPrice = orginalPrice * 7 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 8) {
      discountPrice = 78;
      totalPrice = orginalPrice * 8 - discountPrice + 'kr';
      newPrice = orginalPrice * 8 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 9) {
      discountPrice = 135;
      totalPrice = orginalPrice * 9 - discountPrice + 'kr';
      newPrice = orginalPrice * 9 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 10) {
      discountPrice = 180;
      totalPrice = orginalPrice * 10 - discountPrice + 'kr';
      newPrice = orginalPrice * 10 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 20) {
      discountPrice = 380;
      totalPrice = orginalPrice * 20 - discountPrice + 'kr';
      newPrice = orginalPrice * 20 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    
    if(range >= 30) {
      discountPrice = 600;
      totalPrice = orginalPrice * 30 - discountPrice + 'kr';
      newPrice = orginalPrice * 30 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 40) {
      discountPrice = 920;
      totalPrice = orginalPrice * 40 - discountPrice + 'kr';
      newPrice = orginalPrice * 40 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 50) {
      discountPrice = 1250;
      totalPrice = orginalPrice * 50 - discountPrice + 'kr';
      newPrice = orginalPrice * 50 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 100) {
      discountPrice = 2900;
      totalPrice = orginalPrice * 100 - discountPrice + 'kr';
      newPrice = orginalPrice * 100 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }

    if(range >= 200) {
      discountPrice = 6600;
      totalPrice = orginalPrice * 200 - discountPrice + 'kr';
      newPrice = orginalPrice * 200 + 'kr';
      showDiscount.addClass('discount-color');
      $('.price > span').html(newPrice);
      $('.your-price > span').html(totalPrice);
      $('.discount > span').html(discountPrice + 'kr');
    }
  },

  multiProductAddToCart : function() {
    
    $('.multiproduct-button').on('click', function(e){
      let pID = $(this).data('pid');
      let url = window.location.protocol + '//' + window.location.host + '/?add-to-cart=' + pID;  
      window.location.replace(url);
    });
  },

  removeFromCart : function() {
    $('.product-remove > a.remove').on('click', function() {
      let pID = $(this).data('product_id');
      /** Change ID here incase you add or remove product */
      if(pID !== 420 && pID !== 353 && pID !== 349 && pID !== 347 && pID !== 345 && pID !== 343 && pID !== 341 && pID !== 337) {
        const data = {
          'action' : 'remove_from_cart',
          'product-id' : pID
        }
        $.post(ajaxurl, data, function(result) {
          // Return callback
        });
      }
    }) 
  },

  updateCmAndAmount : function(img, minheight, minwidth) {
    /**
     * Each time we change our cm to something new. 
     * Update the X cm value and give us X amount of images based on CM
     */

    if(img === undefined && minheight === undefined && minwidth === undefined) {
      return;
    }

    else {
      var path = img;
      $('input[name=range-cm]').on('change', function() {
        var valueFromRangeCM = $(this).val();
        $('.cm > .how-wide').html(valueFromRangeCM+'cm');
        var amount_cm = Math.floor(20.1/(minwidth*valueFromRangeCM+0.1)) * Math.floor(20.1/(minwidth * minheight*valueFromRangeCM+0.1));
        $('.amounts > .x-amount').html(amount_cm);
        $('.box__preview-image').remove();
        
        for (var i = 1; i <= amount_cm; i++) {
          $('.box__container__preview-image').append('<img src="'+path+'" class="box__preview-image">');
          $('.box__preview-image').css('height', valueFromRangeCM/201*1000+'%').css('width', valueFromRangeCM/201*1000+'%');
        };
      });
    }
  },

  addToCart : function(file) {
    /**
     * Adds to cart product
     */
    $('.add-to-cart-button').on('click', function() {
      let fd = new FormData();
      fd.append('file', file);
      fd.append('cm', $('input[name=range-cm]').val());
      fd.append('action', 'add_to_cart');
      $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        success: function(pid) {
          $('.box__adding').addClass('show');
          imageSheetMainFunction.productAdded(pid);
        },
      }); 
    });
  },

  productAdded : function(pid) {
    $.get('/wp/?post_type=product&add-to-cart=' + pid, function() {
      $('.box__adding').removeClass('show');
      let url = window.location.protocol + '//' + window.location.host + '/cart'
      window.location.replace(url);
    });
  },

  dragNDrop : function() {
    /**
     * Checkup so we can start drag n drop images in here!
     */

    if (isAdvancedUpload) {
      $form.addClass('has-advanced-upload border');
      
      $fileInput.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
        e.preventDefault();
        e.stopPropagation();
      })
      .on('dragover dragenter', function() {
        $form.addClass('is-dragover');
      })
      .on('dragleave dragend drop', function() {
        $form.removeClass('is-dragover');
      })
      .on('change', function(e) {
        $('label.trigger-open-file-input').addClass('hide-me');
        $('.box__uploading').addClass('show');

          /**
           * If you somehow when you wanted to upload a file pressed cancel then dont start the upload proccess
           */
          
          $('.box__empty-image').removeClass('show');
          var file = $(this)[0].files[0];
          imageSheetMainFunction.addToCart(file);
          img.src = _URL.createObjectURL(file);
          if(typeof(img.src) == 'string') {
          img.onload = function() {
            
            /** 
             * Checkups before we can use these info
             */
            if(file.size > 500000)  {
              alert('File to big! Max 5mb only!');
              return;
            }
            /*
            if(file.type != 'image/jpeg' || file.type != 'image/png') {
              alert("Only image type of jpg/png");
              return;
            }*/
            imgheight = this.height;
            imgwidth = this.width;
            /**
             * Stock width and height = 18x18cm
             */
            minheight = (imgheight / imgwidth * 20)/20;
            minwidth = 20/20;
            imageSheetMainFunction.updateCmAndAmount(img.src, minheight, minwidth);
            /**
             * Stock width and height = 18x18cm
             */
            $('.box.has-advanced-upload').css('height', 'auto');
            $('.box__uploading').removeClass('show');
            $('.box__container__preview-image').addClass('show');
            var range_value = $('input[name=range-cm]').val();
            $('.box__preview-image').attr('src', img.src).css('height', range_value/201*1000+'%').css('width', range_value/201*1000+'%');


            /** Display tools and add to cart button */

            $('.box__tools').addClass('show');
            $('.add-to-cart').addClass('show');
            }
          } else {
          console.log('Failed');
          $('.box__uploading').removeClass('show');
          $('.box__empty-image').addClass('show');
        }
        
      })
      .on('drop', function(e) {

        /**
         * Using the droppedFiles we can get the property of our fake path of dropped file
         */
        let droppedFiles = e.originalEvent.dataTransfer.files;
        $form.find('input[type="file"]').prop('files', droppedFiles);
      });      
    }
  },

  startUp : function() {
    /** Debug text */
    console.log("image sheets javascript loaded");
  }
}

$(document).ready(imageSheetMainFunction.onReady);