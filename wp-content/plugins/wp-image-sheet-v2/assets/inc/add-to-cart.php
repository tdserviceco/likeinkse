<?php
$cm = $_POST['cm'];
// $name_only is found in image-upload.php
include 'image-upload.php';
$order_desc = 'Customer order of %s';
$order = $name_only[1].'-'.$image . ' - ' . $cm . 'cm';
$post_id = wp_insert_post( array(
  'post_title' => $name_only[1] .'-'. $image,
  'post_content' => sprintf (__($order_desc, 'likeink'), $order),
  'post_status' => 'publish',
  'post_type' => "product",
) );

wp_set_object_terms( $post_id, 'simple', 'product_type' );      
update_post_meta( $post_id, '_price', '98' );
update_post_meta( $post_id, '_regular_price', '98' );
update_post_meta( $post_id, '_visibility', 'visible' );

      
// Check that the nonce is valid, and the user can edit this post.
if (isset($post_id)) {
  // The nonce was valid and the user has the capabilities, it is safe to continue.

  // These files need to be included as dependencies when on the front end.
  require_once( ABSPATH . 'wp-admin/includes/image.php' );
  require_once( ABSPATH . 'wp-admin/includes/file.php' );
  require_once( ABSPATH . 'wp-admin/includes/media.php' );
  
  // Let WordPress handle the upload.
  // Remember, 'my_image_upload' is the name of our file input in our form above.
  $filetype = wp_check_filetype($imageDIR); // Get the mime type of the file
  $attachment = array( // Set up our images post data
    'guid'           => $imageDIR,
    'post_mime_type' => $filetype['type'],
    'post_title'     => $name_only[1].'-'.$image,
    'post_author'    => 1,
    'post_content'   => ''
  );
  $attach_id = wp_insert_attachment( $attachment, $imageDIR, $post_id ); // Attach/upload image to the specified post id, think of this as adding a new post.
  $attach_data = wp_generate_attachment_metadata( $attach_id, $imageDIR );
  wp_update_attachment_metadata( $attach_id, $attach_data );
  add_post_meta($post_id, '_thumbnail_id', $attach_id);  
}
echo $post_id;
