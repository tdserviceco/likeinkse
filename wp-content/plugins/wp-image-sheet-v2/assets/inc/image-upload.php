<?php
$tmp_name =  $_FILES["file"]["tmp_name"];
$image = $_FILES['file']['name'];
$name_only = explode('/tmp/', $tmp_name);

$upload_path = wp_upload_dir();
$imageDIR = $upload_path['basedir'] . '/wp-image-sheet/'.$name_only[1].'-'.$image;

//$newpath = $upload_path['basedir'] . '/wp-image-sheet/'.$name_only[1].'.'.$extention[1]);
if (is_dir($upload_path['basedir'].'/wp-image-sheet' ) && is_writable($upload_path['basedir'].'/wp-image-sheet' )):
  move_uploaded_file($tmp_name, $imageDIR);
else:
  echo 'Upload directory is not writable, or does not exist.';
endif;