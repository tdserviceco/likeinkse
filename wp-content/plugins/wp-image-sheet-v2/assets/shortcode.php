<?php 
/**
 * Layout for preview of image 
 */
?>
<form class="box center" method="post" action="" enctype="multipart/form-data">
  <div class="box__container">
    <div class="box__input">
      <input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" />
      <label class="trigger-open-file-input" for="file">
        <strong><?php _e('Choose a file', 'likeink')?></strong>
        <span class="box__dragndrop"> <?php _e('or drag it here','likeink'); ?></span>.
      </label>
      <button class="box__button" type="submit">Upload</button>
      <div class="box__container__preview-image">
        <img src="#" class="box__preview-image" alt="<?php _e('Preview image', 'likeink'); ?>">
      </div>
    </div>
    <div class="box__uploading"><?php _e('Uploading','likeink'); ?><span>.</span><span>.</span><span>.</span></div>
    <div class="box__adding"><?php _e('Adding to cart','likeink'); ?><span>.</span><span>.</span><span>.</span></div>
  </div>
</form>

<?php 
/**
 * Tools and buttons
 */
?>

<div class="box__tools center">
  <div class="box__tools__grid">
    <div class="range">
      <input type="range" min="1" max="18" name="range-cm" value="18" placeholder="18">
    </div>
    <div class="cm">
      <span class="how-wide">18cm</span>
    </div>
    <div class="amounts">
      <span class="x-amount">1</span>
    </div>
  </div>
</div>

<div class="add-to-cart center">
  <button type="button" class="add-to-cart-button center-small"><?php _e('Add to cart', 'likeink'); ?></button>
</div>