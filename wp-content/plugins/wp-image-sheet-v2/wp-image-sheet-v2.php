<?php
/**
 * Plugin name: WP image sheet V2
 * Description: Version 2 of WP image sheet (same result code is different)
 * Author: Tommy Danielsson
 * Version: 2.0a
 */


class WPIS_V2 {
  
  static function init() {
      // Admin stuff

      // Other stuff
      add_shortcode( 'image_sheet', [__CLASS__, 'shortcode_handler'] );
      add_action( 'wp_enqueue_scripts', [__CLASS__, 'scripts'] );
      add_action( 'wp_ajax_remove_from_cart', [__CLASS__, 'remove_from_cart'] );
      add_action( 'wp_ajax_nopriv_remove_from_cart', [__CLASS__, 'remove_from_cart'] );
      add_action( 'wp_ajax_add_to_cart', [__CLASS__, 'add_to_cart'] );
      add_action( 'wp_ajax_nopriv_add_to_cart', [__CLASS__, 'add_to_cart'] );
  }


  /**
   * Here we send product id and remove it from product.
   */
  static function remove_from_cart() {
    if(DOING_AJAX):
      include_once 'assets/inc/remove-from-cart.php';
    endif;
    wp_die();
  }
  
  /**
   * Here we create our own product id and add it to product.
   */
  
   static function add_to_cart() {
    if(DOING_AJAX):
      include_once 'assets/inc/add-to-cart.php';
    endif;
    wp_die();
  }

  static function scripts() {
    wp_enqueue_script('js-main', plugins_url( 'assets/js/image-sheet.js', __FILE__), ['jquery', 'jquery-ui-draggable', 'jquery-ui-droppable'], '0.0.1', true);
  }
 
  static function shortcode_handler() {
    include_once 'assets/shortcode.php';
  }

}

WPIS_V2::init();