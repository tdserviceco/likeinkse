let globalImgHeight;
let globalImgWidth;
let globalUrlSrcImage;
let _URL = window.URL || window.webkitURL;

const img = new Image();

let imageSheetMainFunction = {

  onReady: function () {
    imageSheetMainFunction.startUp(),
    imageSheetMainFunction.imageUploader(),
    imageSheetMainFunction.informationUpdate(),
    imageSheetMainFunction.addToCart(),
    imageSheetMainFunction.removeCustomProduct()
  },

  /** 
   * Since we create our custom product via addToCart 
   * we need a way to remove them also otherwise we create 
   * X products
   */
  
  removeCustomProduct : function() {
    $('.product-remove > a.remove').on('click', function() {
      const data = {
        'action' : 'ajax_remove_product_from_cart_and_WC',
        'product-id' : $('.product-remove > a.remove').data('product_id')
      }
      $.post(ajaxurl, data, function(result) {
        console.log(result);
      });
    }) 
  },


  /** 
   * We create after we click our add to cart link our own 
   * custom product(s) and then we send it to cart.
   */

  addToCart : function() {
    $('.add-to-cart-custom-button').on('click', function(e){
      e.preventDefault();
      let fd = new FormData();
      fd.append('file', $('input[name=file]').prop('files')[0]);
      fd.append('cm', $('input[name=cm]').val());
      fd.append('action', 'add_to_cart');
      $.ajax({
  
        url: ajaxurl,
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        success: function(result) {
          /**
           * Collect our id and send us to cart with our product.
           */
          console.log('result: ', result);
          window.location.href = window.location.origin+'?add-to-cart='+result;
          }
        }); 

    });
  },
  
  /**
   * This is our preview and upload 
   * at the same time. We upload the image so we can preview the image 
   * (reduntent I know i hope V2 can be better!).
   */

  imageUploader : function() {
    
  $('#file').change(function () {
   
    $('.tools').removeClass('valid');
    $('.tools').addClass('valid');
    
    var file = $(this)[0].files[0];
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
      $('.preview-image').empty();
      $('.images-container').empty();
      imgwidth = this.width;
      imgheight = this.height;
      $("#width").text(imgwidth+'px').append('<span> X </span>');
      $("#height").text(imgheight+'px');
      $('.preview-image').attr('src', img.src);
      // @Todo: Please change the width of this image when Design is in place!
      $('.preview-image').css('width',($('input[name=cm]').val() * 37.7952755906 )+'px');

      
      let fd = new FormData();
      
      fd.append('file', $('input[name=file]').prop('files')[0]);
      fd.append('action', 'image_sheet');
  
      $.ajax({
  
        url: ajaxurl,
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        success: function(result) {
          console.log('result: ', result)
          }
        }); 

      globalImgWidth = imgwidth
      globalImgHeight = imgheight
      globalUrlSrcImage = img.src;
      }
      
    });
  },

  /**
   * Name indicate that everytime we change our CM we update 
   * our info on how many we can get in a sheet.
   */
  informationUpdate : function() {

    $('input[name=cm]').on('change', function(){
      $('.preview-image').empty();
      $('.images-container').empty();
      $('.ark-range').html($(this).val()+'cm');
      
      
      let fd = new FormData();
      fd.append('file', $('input[name=file]').prop('files')[0]);
      fd.append('action', 'image_values');
      fd.append('height', globalImgHeight);
      fd.append('width',globalImgWidth);
      fd.append('price', $('input[name=price]').val());
      fd.append('cm', $(this).val());
      fd.append('arks', $('input[name=amount]').val());

      $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        success: function(result) {
            const data_result = $.parseJSON(result);
            console.log(data_result);
            $('.images-container').addClass('show');
            $('.images-container').append(data_result.images);
            $('.arks').attr('value', data_result.amount);          
          }
        });
    });
    
  },

  startUp : function() {
  /**
   * We will return an ark(s) of the image 
   * we submited with the values of:
   * Height in CM
   * Width in CM
   * 
   */

  console.log("image sheets javascript loaded");
  }
}

$(document).ready(imageSheetMainFunction.onReady);
