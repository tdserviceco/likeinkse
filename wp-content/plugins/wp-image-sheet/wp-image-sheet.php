<?php

/**
 * Plugin name: WP image sheet
 * Description: Turn a image into sheets. 
 * Author: Tommy Danielsson
 * Version: 1.0
 */

class WPIS {

  static function init() {
    add_shortcode( 'image_sheet', [__CLASS__, 'image_sheet_shortcode_handler'] );
    add_action('wp_enqueue_scripts', [__CLASS__, 'image_sheet_script']);
    add_action('wp_ajax_image_sheet', [__CLASS__, 'ajax_image_sheet']);
    add_action('wp_ajax_nopriv_image_sheet', [__CLASS__, 'ajax_image_sheet']);
    add_action('wp_ajax_image_values', [__CLASS__, 'ajax_image_values']);
    add_action('wp_ajax_nopriv_image_values', [__CLASS__, 'ajax_image_values']);
    add_action('wp_ajax_add_to_cart', [__CLASS__, 'ajax_add_to_cart']);
    add_action('wp_ajax_nopriv_add_to_cart', [__CLASS__, 'ajax_add_to_cart']);
    add_action('wp_ajax_ajax_remove_product_from_cart_and_WC', [__CLASS__, 'ajax_remove_product_from_cart_and_WC']);
    add_action('wp_ajax_nopriv_ajax_remove_product_from_cart_and_WC', [__CLASS__, 'ajax_remove_product_from_cart_and_WC']);
  }


  /**
   * Add product via add to cart button
   */

  static function ajax_add_to_cart() {
    if(DOING_AJAX):
      $cm = $_POST['cm'];
      $image = $_FILES['file']['name'];
      $upload_path = wp_upload_dir();
      $imageDIR = $upload_path['base_url'] . '/wp-image-sheet/'.$image;
      $order_desc = 'Customer order of %s';
      $order = $image . ' - ' . $cm . 'cm';
      $post_id = wp_insert_post( array(
        'post_title' => $image,
        'post_content' => sprintf (__($order_desc, 'likeink'), $order),
        'post_status' => 'publish',
        'post_type' => "product",
      ) );
    
      wp_set_object_terms( $post_id, 'simple', 'product_type' );      
      update_post_meta( $post_id, '_price', '89' );
      update_post_meta( $post_id, '_regular_price', '89' );
      update_post_meta( $post_id, '_visibility', 'visible' );

            
      // Check that the nonce is valid, and the user can edit this post.
      if (isset($post_id)) {
        // The nonce was valid and the user has the capabilities, it is safe to continue.

        // These files need to be included as dependencies when on the front end.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        
        // Let WordPress handle the upload.
        // Remember, 'my_image_upload' is the name of our file input in our form above.
        $filetype = wp_check_filetype($imageDIR); // Get the mime type of the file
        $attachment = array( // Set up our images post data
          'guid'           => $imageDIR,
          'post_mime_type' => $filetype['type'],
          'post_title'     => $image,
          'post_author'    => 1,
          'post_content'   => ''
        );
        $attach_id = wp_insert_attachment( $attachment, $imageDIR, $post_id ); // Attach/upload image to the specified post id, think of this as adding a new post.
        $attach_data = wp_generate_attachment_metadata( $attach_id, $imageDIR );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        add_post_meta($post_id, '_thumbnail_id', $attach_id);  
      }
      echo $post_id;
    endif;
    wp_die();
  }

  /**
   * Here we send product id and remove it from product.
   */
  static function ajax_remove_product_from_cart_and_WC() {
    if(DOING_AJAX):
      $post_id = $_POST['product-id'];
      $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      $upload_path = wp_upload_dir();
      $image_attribute = wp_get_attachment_image_src( $attachment_id = $post_thumbnail_id);
      $attachment_title = get_the_title($post_thumbnail_id);
      if(!file_exists($upload_path['basedir'] . '/wp-image-sheet/' . $attachment_title)):
        echo $upload_path['basedir'] . '/wp-image-sheet/' . $attachment_title;
      else:
        unlink($upload_path['basedir'] . '/wp-image-sheet/' . $attachment_title); 
        echo "unlink successful";
      endif;
      wp_delete_attachment($post_thumbnail_id, true);
      wp_delete_post($post_id);
    endif;
    wp_die();
  }


  /**
   * This one is for when slider change value 
   * we send via ajax new value back
   */

   static function ajax_image_values() {
    if(DOING_AJAX):
      session_start();
  
      $price = $_POST['price'];
      $width = $_POST['width'];
      $height = $_POST['height'];
      $cm = $_POST['cm'];

      $_SESSION['cm'] = $cm;
      
      $image = $_FILES['file']['name'];
      $_SESSION['filename'] = $image;
      $upload_path = wp_upload_dir();
     
      $minheight = ($height / $width * 20)/20;
      $minwidth = 20/20;
      $amount_cm = floor(20.1/($minwidth*$cm+0.1)) * floor(20.1/($minwidth * $minheight*$cm+0.1));
      
      for ($x = 1; $x <= $amount_cm; $x++) {
        $images .= "<img class='preview-image' style='width:". ($cm/201*1000) ."%' src='".$upload_path['basedir'].'/wp-image-sheet/'.$image."'>";
      };
      
      $array_of_values = [
        'images' => $images,
        'amount' => $amount_cm
      ];
      $_SESSION['amount'] = $array_of_values['amount'];
      echo json_encode($array_of_values);
    endif;
    wp_die();
  }


  /**
   * Here we collect all information we 
   * have on our image and send it to a new folder inside upload (Word Press own)
   */

  static function ajax_image_sheet() {
    if(DOING_AJAX):
      $name = basename($_FILES["file"]["name"]);

      $target = wp_upload_dir();
      $upload_path = $target['basedir'].'/wp-image-sheet/'.$name;
      $tmp_name =  $_FILES["file"]["tmp_name"];
      if (is_dir($target['basedir'].'/wp-image-sheet' ) && is_writable($target['basedir'].'/wp-image-sheet' )):
        move_uploaded_file($tmp_name, $upload_path);
        echo "Upload Complete";
      else:
        echo 'Upload directory is not writable, or does not exist.';
      endif;
    endif;
    wp_die();
  }  

  static function image_sheet_shortcode_handler() {
    include_once 'assets/shortcode.php';
  }
  

  static function image_sheet_script() {
    wp_enqueue_script('js-main', plugins_url( 'js/image-sheet.js', __FILE__), ['jquery'], '0.0.1', true);
    wp_enqueue_style( 'image-sheet-css', plugins_url( 'css/image-sheet.css', __FILE__ ) );
  }
  
}


WPIS::init();