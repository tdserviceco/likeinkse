<form method="post" id="image-size-validation" enctype="multipart/form-data">
  <div class="images-container">
    <img class="preview-image" src="#">
  </div>
  <img class="preview-image" src="<?php echo plugins_url('img/order-description.png', __FILE__)?>" alt="Order Description" />
  <input type="file" id="file" name="file" />
  <div class="tools">
    <?php echo __('Antal per ark', 'likeink') . ':'; ?> <input type="number" class="arks" name="amount" value="1" readonly>
    <input type="range" name="cm" min="1" max="18" value="13"/><span class="ark-range">13cm</span>
    <div class="measurment">
      <span id="width"></span>
      <span id="height"></span>
    </div>
  </div>
  <button type="button"><?php _e('Add more', 'likeink'); ?></button>
</form>
<a class="add-to-cart-custom-button" href="#">Add to cart</a>

