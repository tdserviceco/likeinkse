<?php
$text = get_option('cookie_accept');
$url = get_option('cookie_law_link');
?>

<div class="cookie-container">
  <div class="cookie-text">
    <p><?php echo $text; ?><a href="<?php echo $url['link']; ?>"><?php echo $url['title']; ?></a></p><button id="accept-cookie-button" class="button" type="button"><?php _e('I accept','likeink'); ?></button>
  </div>
</div>
