<?php
/**
 * Plugin name: WP Cookie Text
 * Description: Simple Custom Text for GDPR/Cookie text
 * Author: Tommy Danielsson
 * Version: 1.0a
 */


class WPCT {
  
  static function init() {
    // Admin stuff

    // Other stuff
    add_shortcode( 'wp_cookie_text', [__CLASS__, 'shortcode_handler'] );
    add_action( 'wp_enqueue_scripts', [__CLASS__, 'scripts'] );
    add_action( 'admin_menu', [__CLASS__, 'settings_menu'] );
  }

  static function settings_menu() {

    //create new top-level menu
    add_menu_page('WP cookie text settings', 'WP cookie text settings', 'administrator', __FILE__, [__CLASS__, 'plugin_settings_page'] );
  }

  static function plugin_settings_page() {
    include_once 'assets/settings.php';
  }


  static function scripts() {
    wp_enqueue_script('cookie-text-main', plugins_url( 'assets/js/cookie-law-text.js', __FILE__), ['jquery'], '0.0.1', true);
  }
 
  static function shortcode_handler() {
    include_once 'assets/shortcode.php';
  }

}

WPCT::init();