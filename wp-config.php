<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WzYOTI2OqL+SOd+Eztnl88u0qV+G7Gcz3UPRO1zpnznPjKMUpHOVRF3SAeNN3ueCe8VgzGbbRWI8rpLHLrqqYw==');
define('SECURE_AUTH_KEY',  'UX1buCj0FXziCQvVMuGg/OPZ2rk+fqPS4ypjxy/WxMmuBReUazAYQahMARFAOFkD1dWxcbxU0nXVc+nTFPog7Q==');
define('LOGGED_IN_KEY',    'm39rM2vMHowXU22+/ORdA4QH3iBuqrh/a5SO48hKoFugezDTDYrtjD4ySff6a6ZYE5kGT5qibR276J2BACzh+A==');
define('NONCE_KEY',        'djdz2syEqx8c2mrJ4mBKXLv68fw+brh0jorvYrvbzCbS70F7I3E90BSNCVT+EXe3L6oJ2sRkw9POpZTgsCJIDQ==');
define('AUTH_SALT',        'BVo+CvqTEzmxTONciW134NBHRG0dKa1e6SUtpW3qYf/jlrPpRJ0askP6GVKyuKRe4mZWA2moF2gCMqU1t4dr6w==');
define('SECURE_AUTH_SALT', 'tmxmXp8lD17Ie7HjGrJFuSxFGOngeUpmgVOvGOuxQl9jLmLLIB2HfEgReeHbF8fGrleRexTwfy1g2ZnmrhV7Fg==');
define('LOGGED_IN_SALT',   'c8+NSODuoTMmdQ+QQ8CRr9zl00EujMT3KTkjDlGQmeiXWFJCvTOZmh434xNHK/qrOy5WQEAmuBRdqUOGYgM4YA==');
define('NONCE_SALT',       'Hit/ltlNndN5cBn1WVJcMfeBAcKRTZe6ZkGqZ0mRl2LpbMha3MshfHy79HBe2nDn1Unp37JNS3N57aqqSdeEsw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define('wp_debug', true);


/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if ( ! empty( $_SERVER['SERVER_SOFTWARE'] ) && strpos( $_SERVER['SERVER_SOFTWARE'], 'Flywheel/' ) !== false ) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
